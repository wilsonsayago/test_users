package com.service.user.client.controllers;

import com.service.user.bussiness.globalUtils.Encryption;
import com.service.user.bussiness.services.impl.UserServiceImpl;
import com.service.user.data.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private Encryption encryption;

    @GetMapping
    public ResponseEntity<Map<String, Object>> getAll(@RequestParam(value = "status", required = false) Boolean status){
        logger.info("get all users");
        Map<String, Object> response = new HashMap<>();
        List<String> errors = new ArrayList<>();
        try {
            List<User> users = status != null ? userServiceImpl.getAllByStatus(status) : userServiceImpl.getAll();
            response.put("users", users);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response.put("message", errors);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody User user, BindingResult result){
        logger.info("Saving User");
        Map<String, Object> response = new HashMap<>();
        List<String> errors = new ArrayList<>();
        if (result.hasErrors()) {
            errors = result.getFieldErrors().stream().map(e ->
                    e.getDefaultMessage()
            ).collect(Collectors.toList());
        }
        if (userServiceImpl.getByEmail(user.getEmail()) != null) {
            logger.error("Error: The email already registered");
            errors.add("The email already registered");
        }
        if (!encryption.validatePassword(user.getPassword())) {
            logger.error("Error: The password must have an one uppercase, lowercase letters and two numbers");
            errors.add("The password must have an one uppercase, lowercase letters and two numbers.");
        }
        if(errors.size() > 0) {
            response.put("message", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        userServiceImpl.save(user);
        response.put("user", user);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
