package com.service.user.bussiness.globalUtils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class Encryption {

    private static final Logger logger = LoggerFactory.getLogger(Encryption.class);

    @Value("${secret.key}")
    private String secretKey = null;

    @Value("${time.life.token}")
    private Integer timeLifeToken = null;

    public boolean validatePassword(String password) {
        logger.info("validation password.");
        if (password == null) {
            logger.info("password can't be null.");
            return false;
        } else if (!password.matches(".*[a-z]+.*")) {
            logger.info("password don't have lowercase letters.");
            return false;
        } else if (!password.matches(".*[A-Z]+.*")) {
            logger.info("password don't have one uppercase letters.");
            return false;
        } else if (!password.matches(".*[0-9]+.*")) {
            logger.info("password don't have numbers");
            return false;
        } else if (password.matches("^(?=(.*[A-Z]){2})\\S*$") || password.matches("^(?=(.*[0-9]){3})\\S*$")) {
            logger.info("password can't have to two uppercase letter and/or three or more numbers.");
            return false;
        }
        logger.info("password valid.");
        return true;
    }

    public String generateToken(UUID id, String name) {
        logger.info("generating token.");
        try {
            String token = Jwts
                    .builder()
                    .setId(id.toString())
                    .setSubject(name)
                    .setIssuedAt(new Date(System.currentTimeMillis()))
                    .setExpiration(new Date(System.currentTimeMillis() + timeLifeToken))
                    .signWith(SignatureAlgorithm.HS512,
                            secretKey.getBytes()).compact();
            logger.info("token generated to {}", name);
            return token;
        } catch(Exception e) {
            logger.error(e.getMessage());
            return "";
        }
    }
}
