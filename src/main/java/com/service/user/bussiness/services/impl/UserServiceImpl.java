package com.service.user.bussiness.services.impl;

import com.service.user.bussiness.globalUtils.Encryption;
import com.service.user.bussiness.services.UserService;
import com.service.user.data.entity.User;
import com.service.user.data.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhoneServiceImpl phoneServiceImpl;

    @Autowired
    private Encryption encryption;

    public UserServiceImpl() {
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAll() {
        logger.info("get all users.");
        return (List<User>) userRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAllByStatus(Boolean status) {
        logger.info("get all users by status.");
        return userRepository.findAllByIsActive(status);
    }

    @Override
    @Transactional(readOnly = true)
    public User getById(UUID id) {
        logger.info("get user by id.");
        return userRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public User save(User user) {
        logger.info("save user.");
        userRepository.save(user);
        phoneServiceImpl.save(user);
        user.setToken(encryption.generateToken(user.getId(), user.getName()));
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public User getByEmail(String email) {
        logger.info("get user by email.");
        return userRepository.findByEmail(email);
    }
}
