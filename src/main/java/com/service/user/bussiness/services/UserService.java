package com.service.user.bussiness.services;

import com.service.user.data.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<User> getAll();
    List<User> getAllByStatus(Boolean status);
    User getById(UUID id);
    User save(User user);
    User getByEmail(String email);
}
