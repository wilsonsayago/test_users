package com.service.user.bussiness.services.impl;

import com.service.user.data.entity.User;
import com.service.user.data.repository.PhoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(PhoneServiceImpl.class);

    @Autowired
    private PhoneRepository phoneRepository;

    public void save(User user) {
        logger.info("save phones.");
        user.getPhones().forEach( (phone) -> { phone.setUser(user); });
        phoneRepository.saveAll(user.getPhones());
    }
}
