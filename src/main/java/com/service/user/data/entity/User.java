package com.service.user.data.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class User extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    @NotNull(message = "Name can not be empty")
    private String name;

    //Validacion con dominio .cl: ^[^@]+@[^@]+\.cl$
    //Validacion con cualquier dominio: ^[^@]+@[^@]+\\.[a-zA-Z]{2,}$
    @Email(regexp = "^[^@]+@[^@]+\\.cl$", message = "Email do not have the format accepted")
    @Column(nullable = false, unique = true)
    @NotNull(message = "Email can not be empty")
    private String email;

    @Column(nullable = false)
    @NotNull(message = "Password can not be empty")
    private String password;

    private boolean isActive;

    @Valid
    @OneToMany(mappedBy="user", fetch = FetchType.LAZY, cascade= CascadeType.ALL)
    List<Phone> phones = new ArrayList<>();

    @Column(name = "last_login")
    @JsonProperty("last_login")
    private LocalDateTime lastLogin;

    private String token;

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @PrePersist
    @Override
    public void prePersist() {
        LocalDateTime date = LocalDateTime.now();
        created = date;
        modified = date;
        lastLogin = date;
        isActive = true;
    }
}
