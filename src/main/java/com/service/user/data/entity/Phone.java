package com.service.user.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "phones")
public class Phone extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "Number can not be empty")
    @Column(nullable = false)
    private String number;

    @NotNull(message = "The city code of the phone cannot be empty")
    @Column(nullable = false)
    private String cityCode;

    @NotNull(message = "The country code of the phone cannot be empty")
    @Column(nullable = false)
    private String contryCode;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    public Phone() {
    }

    @JsonIgnore
    @Override
    public UUID getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String citycode) {
        this.cityCode = citycode;
    }

    public String getContryCode() {
        return contryCode;
    }

    public void setContryCode(String contryCode) {
        this.contryCode = contryCode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonIgnore
    @Override
    public LocalDateTime getModified() {
        return modified;
    }

    @JsonIgnore
    @Override
    public LocalDateTime getCreated() {
        return created;
    }
}
