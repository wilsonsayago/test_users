package com.service.user.data.repository;

import com.service.user.data.entity.Phone;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface PhoneRepository extends CrudRepository<Phone, UUID> {
}
