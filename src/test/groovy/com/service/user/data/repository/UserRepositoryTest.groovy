package com.service.user.data.repository

import com.service.user.data.entity.User
import spock.lang.Shared
import spock.lang.Specification

class UserRepositoryTest extends Specification {

    @Shared
    UserRepository userRepository;

    @Shared
    User user

    def setupSpec() {
        userRepository = Mock(UserRepository)
        user = new User()
        user.setName("Juan Rodriguez")
        user.setActive(true)
        user.setEmail("juan@rodriguez.org")
        user.setPassword("Hunter2")
        userRepository.findAll() >> [user]
        userRepository.findAllByIsActive(true) >> [user]
        userRepository.findAllByIsActive(false) >> []
        userRepository.findByEmail("juan@rodriguez.org") >> user
    }

    def "FindAllByIsActive"() {
        when:
        def usersActives = userRepository.findAllByIsActive(true)
        def usersNotActives = userRepository.findAllByIsActive(false)

        then: "Found actives"
        usersActives.size() == 1

        then: "not Fount actives"
        usersNotActives.size() == 0
    }

    def "FindByEmail"() {
        given:
        def email = "juan@rodriguez.org"

        when:
        def userFound = userRepository.findByEmail(email)

        then: "found by email"
        userFound != null
        userFound.getEmail() == email
    }
}
