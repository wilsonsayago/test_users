package com.service.user.bussiness.services.impl

import com.service.user.bussiness.globalUtils.Encryption
import com.service.user.data.entity.User
import com.service.user.data.repository.UserRepository
import spock.lang.Shared
import spock.lang.Specification

class UserServiceImplTest extends Specification {

    @Shared
    UserServiceImpl userService

    @Shared
    UserRepository userRepository

    @Shared
    PhoneServiceImpl phoneService

    @Shared
    Encryption encryption

    @Shared
    User user

    def setup() {
        userRepository = Mock(UserRepository)
        phoneService = Mock(PhoneServiceImpl)
        encryption = Mock(Encryption)
        userService = new UserServiceImpl(userRepository: userRepository, phoneServiceImpl: phoneService, encryption: encryption)
        user = new User()
        user.setName("Juan Rodriguez")
        user.setActive(true)
        user.setEmail("juan@rodrigueza.cl")
        user.setPassword("Hunter22")
    }

    def "GetAll"() {
        given:
        userRepository.findAll() >> Arrays.asList(user)

        when:
        def usersFound = userService.getAll()

        then:
        usersFound.size() == 1
    }

    def "GetAllByStatus"() {
        given:
        userRepository.findAllByIsActive(true) >> Arrays.asList(user)
        userRepository.findAllByIsActive(false) >> []

        when:
        def usersActives = userService.getAllByStatus(true)
        def usersNoActives = userService.getAllByStatus(false)

        then:
        usersActives.size() == 1
        usersNoActives.size() == 0
    }

    def "GetById"() {
        given:
        def id = UUID.randomUUID()
        user.setId(id)
        userRepository.findById(user.getId()) >> Optional.of(user)

        when:
        def userFound = userService.getById(user.getId())

        then:
        userFound.getName() == user.getName()
    }

    def "Save"() {
        given:
        user.setId(UUID.randomUUID())
        userRepository.save(user) >> user
        encryption.generateToken(user.getId(), user.getName()) >> "test"
        when:
        def userSave = userService.save(user)

        then:
        userSave.getName() == user.getName()
        user.getToken() == "test"
    }

    def "GetByEmail"() {
        given:
        userRepository.findByEmail(user.getEmail()) >> user

        when:
        def userFound = userService.getByEmail(user.getEmail())

        then:
        userFound.getEmail() == user.getEmail()
    }
}
