package com.service.user.bussiness.globalUtils

import spock.lang.Shared
import spock.lang.Specification

class EncryptionTest extends Specification {

    @Shared
    Encryption encryption

    def setup(){
        encryption = new Encryption(secretKey: "test", timeLifeToken: 123)
    }

    def "ValidatePasswordNull"() {
        given:
        def valid
        def passwordNull = null

        when:
        valid = encryption.validatePassword(passwordNull)

        then: "Password null"
        valid == false
    }

    def "ValidatePasswordNotHaveLowercase"() {
        given:
        def passwordNotHaveLowercaseLetter = "HUNTER22"

        when: "Dont have lowercase letters"
        def valid = encryption.validatePassword(passwordNotHaveLowercaseLetter)

        then:
        valid == false
    }

    def ""(){
        given:
        def passwordNotHaveUppercaseLetter = "hunter22"

        when: "Dont have uppercase letter"
        def valid = encryption.validatePassword(passwordNotHaveUppercaseLetter)

        then:
        valid == false
    }


    def "ValidatePasswordNotHaveNumbers"() {
        given:
        def passwordNotHaveNumbers = "Hunter"

        when: "Dont have numbers"
        def valid = encryption.validatePassword(passwordNotHaveNumbers)

        then:
        valid == false
    }

    def "ValidatePasswordNotHaveOneUppercaseLetterAndTwoNumbers"() {
        given:
        def passwordNotHaveOneUppercaseLetterAndTwoNumbers = "HunTer234"

        when: "Dont have one uppercase lettes and/or two numbers"
        def valid = encryption.validatePassword(passwordNotHaveOneUppercaseLetterAndTwoNumbers)

        then:
        valid == false
    }

    def "ValidatePasswordOk"(){
        given:
        def passwordOk = "H2unter2"

        when: "Password Ok"
        encryption.validatePassword(passwordOk) >> !(passwordOk.matches('^(?=(.*[A-Z]){2})\\S*$') || passwordOk.matches('^(?=(.*[0-9]){3})\\S*$'))
        def valid = encryption.validatePassword(passwordOk)

        then:
        valid == true
    }

    def "GenerateTokenOk"() {
        given:
        def id = UUID.randomUUID()
        def name = "test"

        when:
        def token = encryption.generateToken(id, name)

        then:
        token != null
    }

    def "GenerateTokenNull"() {
        given:
        def id = UUID.randomUUID()

        when:
        def token = encryption.generateToken(null, null)

        then:
        token == ""
    }
}
