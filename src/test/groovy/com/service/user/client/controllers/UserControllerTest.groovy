package com.service.user.client.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.service.user.bussiness.globalUtils.Encryption
import com.service.user.bussiness.services.impl.UserServiceImpl
import com.service.user.data.entity.User
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Shared
import spock.lang.Specification

import static org.hamcrest.Matchers.hasSize

class UserControllerTest extends Specification {

    @Shared
    UserController userController

    @Shared
    UserServiceImpl userServiceImpl

    @Shared
    Encryption encryption

    @Shared
    MockMvc mockMvc

    ObjectMapper mapper = new ObjectMapper()

    def urlBase = "/users"

    @Shared
    User user

    String userJsonString

    def setup() {
        userServiceImpl = Mock(UserServiceImpl)
        encryption = Mock(Encryption)

        userController = new UserController([userServiceImpl: userServiceImpl, encryption: encryption])
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
                .alwaysDo(MockMvcResultHandlers.print())
                .build()
        user = new User()
        user.setName("Juan Rodriguez")
        user.setActive(true)
        user.setEmail("juan@rodrigueza.cl")
        user.setPassword("Hunter22")
        userJsonString = mapper.writeValueAsString(user)
    }

    def "GetAll"() {
        given:
        def statusTrue = true
        def statusFalse = false
        userServiceImpl.getAllByStatus(statusTrue) >> [user]
        userServiceImpl.getAllByStatus(statusFalse) >> []
        userServiceImpl.getAll() >> [user]

        expect:
        mockMvc.perform(MockMvcRequestBuilders.get(urlBase + '?status=true'))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.users', hasSize(1)))

        mockMvc.perform(MockMvcRequestBuilders.get(urlBase + '?status=false'))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.users', hasSize(0)))

        mockMvc.perform(MockMvcRequestBuilders.get(urlBase))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$.users', hasSize(1)))
    }

    def "Save"() {
        given:
        userServiceImpl.save(user) >> user
        encryption.validatePassword(user.getPassword()) >> true

        expect:
        mockMvc.perform(
                MockMvcRequestBuilders.post(urlBase)
                .contentType(MediaType.APPLICATION_JSON).content(userJsonString))
        .andExpect(MockMvcResultMatchers.status().isCreated())
    }
}
