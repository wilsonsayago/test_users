# Getting Started

# User Test

### Overview
This is a demo application of a spring boot backend API, stores the information in memory h2 database, and It exposes two REST endpoints, the first allows to get the list of users with a optional status filtering, and also another endpoint to save a new user. 

* The application does not uses any user and/or password in order to authenticate.
* By default, a new database **test** will be created on memory h2 instance.    

### Running your service

```
$ gradle build
$ java -jar build/libs/user-0.0.1-SNAPSHOT.jar
```
After that, your application should be up and running on [localhost:8080](http://localhost:8080/users)

### Tech stack
This is a quick list of the technologies and dependencies used to implement this demo:
* Java, version 1.8
* Apache Tomcat embedded
* Spring Boot, version 2.3.2.RELEASE
    * Spring-boot-starter-data-jpa, version 2.3.2.RELEASE
    * spring-boot-starter-web, version 2.3.2.RELEASE
    * H2, version 2.1.3 RELEASE
    * Spring-boot-starter-test, version 2.3.2.RELEASE
    * Spring-boot-starter-validation, version 2.3.2.RELEASE
* jjwt, version 0.2
* java-jwt, version 2.0.1
* groovy-all, version 2.4.8
* spock-core, version 1.1-groovy-2.4
* spock-spring, version 2.0-M3-groovy-3.0
* springfox-swagger2, version 2.9.2
* springfox-swagger-ui, version 2.9.2

### API documentation
As this demo application uses **Swagger** and **Swagger-ui,** you can easilly view and try the REST endpoints exposed by this backend API.

With the application up and running, you can go to the swagger-ui url in your web browser, by default:
* [Swagger-ui: users](http://localhost:8080/swagger-ui.html#)

The backend API exposes endpoints:
* [GET /users](http://localhost:8080/users)
* [GET /users by status true](http://localhost:8080/users?status=true)
* [GET /users by estatus false](http://localhost:8080/users?status=false)
* [POST /users](http://localhost:8080/users)
